package com.example.nishantghanate.gridlayout;

/**
 * Created by Nishant Ghanate on 31-Mar-18.
 */

class DataStoringClassList {
    public String Fuel;
    public int image ;
    // This const should be public to access from base activity class
    public DataStoringClassList(String fuel, int ic_android) {
        this.Fuel = fuel;
        this.image = ic_android;
    }

    public String getFuel(){ return  Fuel;}

    public int getImage() { return image;}
}
