package com.example.nishantghanate.gridlayout;

import android.content.Context;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Nishant Ghanate on 31-Mar-18.
 */

class GridListAdapter extends RecyclerView.Adapter<GridListAdapter.Holderview>  {
    Context context;
    List<DataStoringClassList> list = new ArrayList<>();
    public GridListAdapter(Context applicationContext, List<DataStoringClassList> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public GridListAdapter.Holderview onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout= LayoutInflater.from(parent.getContext()).
                inflate(R.layout.cardview,parent,false);

        return new Holderview(layout);
    }

    @Override
    public void onBindViewHolder(GridListAdapter.Holderview holder, int position) {
        holder.textView.setText(list.get(position).getFuel());
        holder.imageView.setBackgroundResource(list.get(position).getImage());
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        holder.constraintLayout.setBackgroundColor(color);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holderview extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        CardView cardView;
        ConstraintLayout constraintLayout;

        public Holderview(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardView);
            textView = itemView.findViewById(R.id.textView);
            imageView = itemView.findViewById(R.id.imageView);
            constraintLayout = itemView.findViewById(R.id.constrainLayout);
        }
    }
}
