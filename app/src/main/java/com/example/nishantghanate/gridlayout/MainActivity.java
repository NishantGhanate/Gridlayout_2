package com.example.nishantghanate.gridlayout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    GridListAdapter adapter;
    List<DataStoringClassList> List ;
    RecyclerView.LayoutManager RecyclerViewLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Link Recycle view from base activity
        recyclerView  =  findViewById(R.id.RecycleView);
        // If the size of card is not changing helps in performance
        recyclerView.hasFixedSize();
        // Setting up Grid layout or span Count 2
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        // Call function to store data set
        AddItems();
        // Passing Context and List
        adapter = new GridListAdapter(getApplicationContext(),List);
        recyclerView.setAdapter(adapter);

        /*
        * // For simple Recycle View
        RecyclerViewLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(RecyclerViewLayoutManager);
        AddItems(); // DATA SET
        RecyclerView.Adapter adapter = new RecyclerViewAdapter(SubjectNames,image);
        recyclerView.setAdapter(adapter);
        *
        *
        * */

    }//-------------------------------- End of onCreate-------------------------------------------

    public void AddItems(){
        List = new ArrayList<>();
        for(int i = 0 ; i< 15 ; i++){
            List.add(new DataStoringClassList("Fuel",R.drawable.ic_android));
            List.add(new DataStoringClassList("Fuel",R.drawable.ic_android_grey));
        }



    }


}
